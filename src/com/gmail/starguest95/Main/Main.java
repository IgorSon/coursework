package com.gmail.starguest95.Main;

import com.gmail.starguest95.Gui.War;
import com.gmail.starguest95.InOut.ReadParams;
import com.gmail.starguest95.InOut.ReadTeamConfig;
import com.gmail.starguest95.ShipsUtils.Ship;
import com.gmail.starguest95.ShipsUtils.Torpedo;
import com.gmail.starguest95.SimulationUtils.Actions;

import java.util.ArrayList;
import java.util.List;


public class Main {

    public static List<Ship> TEAM_1 = new ArrayList<Ship>();
    public static List<Ship> TEAM_2 = new ArrayList<Ship>();
    public static int MAX_X = 25;
    public static int MAX_Y = 25;
    public static int MIN_X = 0;
    public static int MIN_Y = 0;
    public static List<Coords> BLOCKED_CELLS = new ArrayList<Coords>();
    public static List<Coords> BLOCKED_CELLS_FOR_TORPEDO = new ArrayList<Coords>();
    public static List<Torpedo> torpedos = new ArrayList<Torpedo>();
    public static Actions act = new Actions();


    public static void main(String[] args) {

        ReadParams.readParams();
        ReadTeamConfig.readTeamConfig();
        Actions.refreshBlockedCells();

        BLOCKED_CELLS_FOR_TORPEDO = BLOCKED_CELLS;

        // Заполняем очередность кораблей
        act.shipOrder();


        // Отрисовка GUI:
        War.dialog.setTitle("Battleships");
        War.dialog.pack();
        War.dialog.setVisible(true);


    }

    public static int checkForWin() {
        boolean firstTeamAlive, secondTeamAlive;
        firstTeamAlive = isTeamAlive(TEAM_1);
        secondTeamAlive = isTeamAlive(TEAM_2);
        if (firstTeamAlive && secondTeamAlive) {
            return 0;
        } else if (firstTeamAlive) {
            return 1;
        } else
            return 2;
    }

    private static boolean isTeamAlive(List<Ship> team) {
        for (Ship s : team) {
            if (s.getDurability() > 0)
                return true;
        }
        return false;
    }

}
