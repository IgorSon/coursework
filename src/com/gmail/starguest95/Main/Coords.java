package com.gmail.starguest95.Main;

import java.util.List;

/**
 * Created by isv on 01.03.17.
 * Координаты объектов на поле
 */
public class Coords {

    private int x = 0;
    private int y = 0;

    public Coords() {
        this.x = 0;
        this.y = 0;
    }

    public Coords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int value) {
        //set X coord
        this.x = value;
    }

    public void setY(int value) {
        //set Y coord
        this.y = value;
    }

    public int getX() {
        //get X coord
        return this.x;
    }

    public int getY() {
        //get Y coord
        return this.y;
    }

    public String getAll() {
        return "( " + this.x + ", " + +this.y + " )";
    }

    public static boolean containsCoords(List<Coords> coordsList, Coords target) {

        //iterate through coordinates list
        for (int i = 0; i < coordsList.size(); i++) {
            //if X value is equals to target X value then
            if (coordsList.get(i).getX() == target.getX()) {
                //if Y value is equals to target Y value then, else continue to iterate
                if (coordsList.get(i).getY() == target.getY()) {
                    return true;
                }
            }
        }

        return false;
    }

    //if coord is valid
    public static boolean isValid(Coords target) {
        if (target.getX() == -1 && target.getY() == -1) {
            return false;
        }
        return true;
    }

}
