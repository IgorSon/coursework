package com.gmail.starguest95.ShipsUtils;

import com.gmail.starguest95.Gui.War;
import com.gmail.starguest95.Main.Coords;
import com.gmail.starguest95.Main.Main;

import java.util.ArrayList;
import java.util.List;

import static com.gmail.starguest95.ShipsUtils.Ship.distance;

/**
 * Created by isv on 10.03.17.
 * Дальнобойное оружие кораблей
 */
public class Torpedo{
    //num from that ID is starting
    private static int ID_STARTER = 0;
    //amount of cells that torpedo move by every iteration
    private final static int MOVE_COEF = 1;

    //max amount of cells that can be passed by torpedo
    public static final int MAX_LIFETIME = 10;
    //aoe damage
    private static final int DAMAGE_AREA = 2;

    //current coordinates of torpedo
    private Coords currentCoords = new Coords();
    //value of damage dealing by torpedo
    private int shootingPower = 100;
    //torpedo ID
    private int ID = 0;
    //speed depends of torpedo
    private int speed = 4;
    //direction of moving: r - right, l - left, u - up, d - down, ur - up right, ul - up left, br - bottom right, bl - bottom left
    private String direction = "";
    //coordinates that torpedo focused on
    private Coords destinationCoords = new Coords();
    //is torpedo alive or gonna explode this turn
    private boolean alive = true;
    //passed cells amount
    private int passedCells = 0;
    //coords of ship that shooted torpedo
    private Coords shooterCoords = new Coords();
    


    public Torpedo(Ship shooter, Coords destination){

        //set new ID for torpedo
        this.setID(returnID());
        //initialize direction and current coords
        this.setDirection(shooter.getDirection());
        this.setCurrentCoords(shooter.getCoords());
        this.setDestinationCoords(destination);
        this.shooterCoords = shooter.getCoords();

        //if this torpedo is already existing
        if (!containsTorpedo(Main.torpedos)) {
            Main.torpedos.add(this);
            ID_STARTER++;
        }

        act();

    }

    private boolean containsTorpedo(List<Torpedo> trpList){
        //iterate through torpedoes list
        for (int i = 0; i < trpList.size(); i++){
            if (trpList.get(i).getID() == this.getID()){
                return true;
            }
        }
        return false;
    }

    //main method that should be run every turn
    public void act() {

        //if torpedo is alive
        if (this.isAlive()) {
            //if torpedo reached max lifetime then explode
            if (this.getPassedCells() == MAX_LIFETIME) {
                explode();
            } else {
                //prerare diagonal direction
                String diagDir = "";
                //move torpedo on one point by every iteration
                for (int speedPoint = 0; speedPoint < this.getSpeed(); speedPoint++) {

                    int xDir = 0, yDir = 0;
                    //first, need to know in which direction ship supposed to go
                    if ((this.getCurrentCoords().getX() < this.getDestinationCoords().getX()) && (this.getCurrentCoords().getY() >= this.getDestinationCoords().getY())) {
                        //right top direction
                        xDir = MOVE_COEF;
                        yDir = -MOVE_COEF;
                        this.setDirection("r");
                        diagDir = "ur";
                    }
                    if ((this.getCurrentCoords().getX() >= this.getDestinationCoords().getX()) && (this.getCurrentCoords().getY() > this.getDestinationCoords().getY())) {
                        //left top direction
                        xDir = -MOVE_COEF;
                        yDir = -MOVE_COEF;
                        this.setDirection("u");
                        diagDir = "ul";
                    }
                    if ((this.getCurrentCoords().getX() > this.getDestinationCoords().getX()) && (this.getCurrentCoords().getY() <= this.getDestinationCoords().getY())) {
                        //left bottom direction
                        xDir = -MOVE_COEF;
                        yDir = MOVE_COEF;
                        this.setDirection("l");
                        diagDir = "bl";
                    }
                    if ((this.getCurrentCoords().getX() <= this.getDestinationCoords().getX()) && (this.getCurrentCoords().getY() < this.getDestinationCoords().getY())) {
                        //right bottom direction
                        xDir = MOVE_COEF;
                        yDir = MOVE_COEF;
                        this.setDirection("d");
                        diagDir = "br";
                    }


                    Coords verticalPoint = new Coords(), horizonPoint = new Coords(), diagonalPoint = new Coords();

                    //prepare points
                    verticalPoint.setX(this.getCurrentCoords().getX());
                    verticalPoint.setY(this.getCurrentCoords().getY() + yDir);
                    horizonPoint.setX(this.getCurrentCoords().getX() + xDir);
                    horizonPoint.setY(this.getCurrentCoords().getY());
                    diagonalPoint.setX(this.getCurrentCoords().getX() + xDir);
                    diagonalPoint.setY(this.getCurrentCoords().getY() + yDir);

                    //calulate the shortest distance to the destination
                    Coords tempCell = new Coords();
                    tempCell = chooseShortest(verticalPoint, horizonPoint, diagonalPoint, this.getDestinationCoords());
                    //if selected coordinate is not valid then, explode
                    if (Coords.isValid(tempCell)) {
                        this.currentCoords = tempCell;
                        //increase passed cells amount
                        this.setPassedCells(this.getPassedCells() + 1);
                    } else {
                        //explode if cell is blocked
                        explode();
                        break;
                    }
                    //if torpedo reached destination coordinates it explodes
                    if (tempCell.getX() == this.getDestinationCoords().getX() &&
                            tempCell.getY() == this.getDestinationCoords().getY()){
                        explode();
                        break;
                    }

                }
            }
        }

    }

    private Coords chooseShortest(Coords verticalPoint, Coords horizonPoint, Coords diagonalPoint, Coords destination){

        Coords outCoord = new Coords();
        List<Coords> outList = new ArrayList<Coords>();
        double minDist = 0;

        outList.add(verticalPoint);
        outList.add(horizonPoint);
        outList.add(diagonalPoint);

        //find minimum distance
        if (outList.size() > 0) {
            minDist = distance(outList.get(0), destination);
            outCoord = outList.get(0);
        }
        for (int i = 0; i < outList.size(); i++){
            if (minDist > distance(outList.get(i), destination)){
                minDist = distance(outList.get(i), destination);
                outCoord = outList.get(i);
            }
        }

        //if choosed coordinate is occupied by object, then make it invalid
        if (Coords.containsCoords(Main.BLOCKED_CELLS_FOR_TORPEDO, outCoord)){
            if  (this.shooterCoords.getX() != outCoord.getX() &&
                    this.shooterCoords.getY() != outCoord.getY()) {
                outCoord.setX(-1);
                outCoord.setY(-1);
            }
        }

        //return calculated coords
        return outCoord;

    }


    private void explode(){
        //if torpedo is alive

        if (this.isAlive()) {
            //DA - damage area
            int mostLeftDAPoint = 0, mostHighDAPoint = 0, mostRightDAPoint = 0, mostDownDAPoint = 0;
            //find left high point of damage area
            mostLeftDAPoint = this.getCurrentCoords().getX() - DAMAGE_AREA;
            mostHighDAPoint = this.getCurrentCoords().getY() - DAMAGE_AREA;
            //find right down point of damage area
            mostRightDAPoint = this.getCurrentCoords().getX() + DAMAGE_AREA;
            mostDownDAPoint = this.getCurrentCoords().getY() + DAMAGE_AREA;

            //if this point is out of war field, then it equals to default
            if (mostLeftDAPoint < Main.MIN_X) {
                mostLeftDAPoint = Main.MIN_X;
            }
            if (mostHighDAPoint < Main.MIN_Y) {
                mostHighDAPoint = Main.MIN_Y;
            }
            if (mostRightDAPoint > Main.MAX_X) {
                mostRightDAPoint = Main.MAX_X;
            }
            if (mostDownDAPoint > Main.MAX_Y) {
                mostDownDAPoint = Main.MAX_Y;
            }

            makeDamage(mostLeftDAPoint, mostHighDAPoint, mostRightDAPoint, mostDownDAPoint, 1);
            makeDamage(mostLeftDAPoint, mostHighDAPoint, mostRightDAPoint, mostDownDAPoint, 2);


            //kill current torpedo
            this.setAlive(false);
            System.out.println("----< Торпеда " + this.getID() + " взорвалась в ("+this.getCurrentCoords().getX()+","+this.getCurrentCoords().getY()+") >-------");
            War.dialog.log.setText(War.dialog.log.getText()+"\n----< Торпеда " + this.getID()+" с силой: "+this.getShootingPower() + " взорвалась в ("+(this.getCurrentCoords().getX()+1)+","+(this.getCurrentCoords().getY()+1)+") >-------");
        }

    }

    private void makeDamage(int mostLeftDAPoint, int mostHighDAPoint, int mostRightDAPoint, int mostDownDAPoint, int teamNum){

        List<Ship> targetList = new ArrayList<Ship>();
        // Какой урон будет нанесен, целый или половина.
        int damage = 0;
        if (teamNum == 1){
            targetList = Main.TEAM_1;
        } else {
            targetList = Main.TEAM_2;
        }

        for (int i = 0; i < targetList.size(); i++){
            Coords coordTemp = new Coords();
            coordTemp.setY(targetList.get(i).getCoords().getY());
            coordTemp.setX(targetList.get(i).getCoords().getX());
            //if ship is in damage area, it takes half damage
            if ((coordTemp.getX() >= mostLeftDAPoint) && (coordTemp.getY() >= mostHighDAPoint)){
                if ((coordTemp.getX() <= mostRightDAPoint) && (coordTemp.getY() <= mostDownDAPoint)){
                    damage = this.getShootingPower() / 2;
                    //if ship is in epicenter then it takes full damage
                    if (this.getCurrentCoords().getX() == targetList.get(i).getCoords().getX()
                            && this.getCurrentCoords().getY() == targetList.get(i).getCoords().getY()){
                        damage = this.getShootingPower();
                    }
                    targetList.get(i).setDurability(targetList.get(i).getDurability() - damage);
                }
            }
        }

        //change ship list
        if (teamNum == 1){
            Main.TEAM_1 = targetList;
        } else {
            Main.TEAM_2 = targetList;
        }

    }


    public int getID() {
        return ID;
    }

    private void setID(int ID) {
        this.ID = ID;
    }

    public int getSpeed() {
        return speed;
    }

    private void setSpeed(int speed) {
        this.speed = speed;
    }

    public Coords getCurrentCoords() {
        return currentCoords;
    }

    private void setCurrentCoords(Coords currentCoords) {
        this.currentCoords = currentCoords;
    }

    public int getShootingPower() {
        return shootingPower;
    }

    private void setShootingPower(int shootingPower) {
        this.shootingPower = shootingPower;
    }

    public String getDirection() {
        return direction;
    }

    private void setDirection(String direction) {
        this.direction = direction;
    }

    public Coords getDestinationCoords() {
        return destinationCoords;
    }

    private void setDestinationCoords(Coords destinationCoords) {
        this.destinationCoords = destinationCoords;
    }

    private static int returnID(){
        return ID_STARTER;
    }

    public boolean isAlive() {
        return alive;
    }

    private void setAlive(boolean alive) {
        this.alive = alive;
    }

    public int getPassedCells() {
        return passedCells;
    }

    public void setPassedCells(int passedCells) {
        this.passedCells = passedCells;
    }
}