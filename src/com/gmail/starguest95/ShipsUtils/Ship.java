package com.gmail.starguest95.ShipsUtils;

import com.gmail.starguest95.Gui.War;
import com.gmail.starguest95.InOut.ReadParams;
import com.gmail.starguest95.Main.Coords;
import com.gmail.starguest95.Main.Main;
import com.gmail.starguest95.SimulationUtils.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by isv on 01.03.17.
 * small ship - |*|, * - key point
 * medium ship - |*|
 * large ship - |*|
 *
 * key point - coords of ship, and ship will rotate around this coords
 */
public class Ship {

    private final static int MOVE_COEF = 1;
    private final static double DISTANCE_WEIGHT = 1;
    private final static double HP_WEIGHT = 0;
    private final static double REPAIR_COEF = 0.25;
    private final static int RAM_RANGE = 1;
    private final static double RAM_SUCCESS_COEF = 1;


    //specialisation: 1 - combat, 2 - repair
    private int specialisation = 0;
    //types: 1 - small ship, 2 - medium ship, 3 - large ship
    private int type = 0;
    //durability of ship depends of type
    private int durability = 0;
    //current coordinates of ship
    private Coords currentCoords = new Coords();
    //value of damage dealing by ship
    private int shootingPower = 0;
    //ship ID
    private int ID = 0;
    //range of detecting enemies
    private int visionRange = 0;
    //range of dealing damage
    private int shootingRange = 0;
    //propability to hit the target
    private int acuracyRate = 0;
    //speed depends of type
    private int speed = 0;
    //direction of moving: r - right, l - left, u - up, d - down, ur - up right, ul - up left, br - bottom right, bl - bottom left
    private String direction = "";
    //ID of target that ship focused on
    private Ship currentTarget = null;
    //torpedo capacity
    private int torpedoCapacity = 0;
    //missile capacity
    private int missileCapacity = 0;
    // Список всех видимых противников
    private List<Ship> targetList = null;

    public int getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(int specialisation) {
        this.specialisation = specialisation;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
        //change parameters of ships that been read from file
        this.setDurability(ReadParams.getDurabilities().get(type - 1));
        this.setAcuracyRate(ReadParams.getAcuracyRateList().get(type - 1));
        this.setShootingPower(ReadParams.getShootingPowerList().get(type - 1));
        this.setShootingRange(ReadParams.getShootingRangeList().get(type - 1));
        this.setVisionRange(ReadParams.getVisionRangeList().get(type - 1));
        this.setSpeed(ReadParams.getSpeedList().get(type - 1));
        this.setTorpedoCapacity(ReadParams.getTorpedoCapacityList().get(type - 1));
        this.setMissileCapacity(ReadParams.getMissileCapList().get(type - 1));
    }

    //replace ship in a repairing class
    public void makeRepairClassShip() {
        this.setAcuracyRate(0);
    }

    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    public Coords getCoords() {
        return currentCoords;
    }

    public void setCoords(Coords newCoords) {
        this.currentCoords = newCoords;
    }

    public void move(Coords destination) {
        //prerare diagonal direction
        String diagDir = "";
        //move ship on one point by every iteration
        for (int speedPoint = 0; speedPoint < this.getSpeed(); speedPoint++) {
            //variable that controls number of twisting around of ship to choose direction
            int directionCounter = 0;
            int xDir = 0, yDir = 0;
            //first, need to know in which direction ship supposed to go
            if ((this.getCoords().getX() < destination.getX()) && (this.getCoords().getY() >= destination.getY())) {
                //right top direction
                xDir = MOVE_COEF;
                yDir = -MOVE_COEF;
                this.setDirection("r");
                diagDir = "ur";
            }
            if ((this.getCoords().getX() >= destination.getX()) && (this.getCoords().getY() > destination.getY())) {
                //left top direction
                xDir = -MOVE_COEF;
                yDir = -MOVE_COEF;
                this.setDirection("u");
                diagDir = "ul";
            }
            if ((this.getCoords().getX() > destination.getX()) && (this.getCoords().getY() <= destination.getY())) {
                //left bottom direction
                xDir = -MOVE_COEF;
                yDir = MOVE_COEF;
                this.setDirection("l");
                diagDir = "bl";
            }
            if ((this.getCoords().getX() <= destination.getX()) && (this.getCoords().getY() < destination.getY())) {
                //right bottom direction
                xDir = MOVE_COEF;
                yDir = MOVE_COEF;
                this.setDirection("d");
                diagDir = "br";
            }

            /**
             * now we can choose one of three points to move on, diagonal, hoizon, vertical
             **for the right top direction it looks like

             |_|_|
             |*|_|

             '*' - is current coordinates of the ship, empty cells candidates to move in

             */

            Coords verticalPoint = new Coords(), horizonPoint = new Coords(), diagonalPoint = new Coords();

            //prepare points
            verticalPoint.setX(this.getCoords().getX());
            verticalPoint.setY(this.getCoords().getY() + yDir);
            horizonPoint.setX(this.getCoords().getX() + xDir);
            horizonPoint.setY(this.getCoords().getY());
            diagonalPoint.setX(this.getCoords().getX() + xDir);
            diagonalPoint.setY(this.getCoords().getY() + yDir);

            //calulate the shortest distance to the destination
            Coords tempCell = new Coords();
            tempCell = chooseShortest(verticalPoint, horizonPoint, diagonalPoint, destination);
            //if selected coordinate is not valid then, check next direction chosen in clock arrow
            if (Coords.isValid(tempCell)) {
                // Лог от дяди Федора
                War.dialog.ships[this.getCoords().getX()][this.getCoords().getY()] = 10;
                System.out.println("Корабль " + this.getID() + " поплыл из (" + this.getCoords().getX() + "," + this.getCoords().getY() + ") в (" + tempCell.getX() + ", " + tempCell.getY() + ") ");
                War.dialog.log.setText(War.dialog.log.getText() + "\nКорабль " + this.getID() + " с HP:" + this.getDurability() + " поплыл из (" + (this.getCoords().getX() + 1) + "," + (this.getCoords().getY() + 1) + ") в (" + (tempCell.getX() + 1) + ", " + (tempCell.getY() + 1) + ") ");

                this.getCoords().setX(tempCell.getX());
                this.getCoords().setY(tempCell.getY());
            } else {
                while (!Coords.isValid(tempCell)) {
                    xDir = diagonalPoint.getX() - this.getCoords().getX();
                    yDir = diagonalPoint.getY() - this.getCoords().getY();

                    //go by clock arrow direction
                    if (xDir == MOVE_COEF && yDir == MOVE_COEF) {
                        //left bottom direction
                        xDir = -MOVE_COEF;
                        yDir = MOVE_COEF;
                        this.setDirection("l");
                        diagDir = "bl";
                    } else if (xDir == -MOVE_COEF && yDir == MOVE_COEF) {
                        //left top direction
                        xDir = -MOVE_COEF;
                        yDir = -MOVE_COEF;
                        this.setDirection("u");
                        diagDir = "ul";
                    } else if (xDir == -MOVE_COEF && yDir == -MOVE_COEF) {
                        //right top direction
                        xDir = MOVE_COEF;
                        yDir = -MOVE_COEF;
                        this.setDirection("r");
                        diagDir = "ur";
                    } else if (xDir == MOVE_COEF && yDir == -MOVE_COEF) {
                        //right bottom direction
                        xDir = MOVE_COEF;
                        yDir = MOVE_COEF;
                        this.setDirection("d");
                        diagDir = "br";
                    }
                    //prepare points
                    verticalPoint.setX(this.getCoords().getX());
                    verticalPoint.setY(this.getCoords().getY() + yDir);
                    horizonPoint.setX(this.getCoords().getX() + xDir);
                    horizonPoint.setY(this.getCoords().getY());
                    diagonalPoint.setX(this.getCoords().getX() + xDir);
                    diagonalPoint.setY(this.getCoords().getY() + yDir);

                    //calculate
                    tempCell = chooseShortest(verticalPoint, horizonPoint, diagonalPoint, destination);


                    if (directionCounter > 3) {
                        break;
                    }
                    directionCounter++;

                }

                //if ship reached destination coordinates, exit from loop
                if (tempCell.getX() == destination.getX() &&
                        tempCell.getY() == destination.getY()) {
                    this.getCoords().setX(destination.getX());
                    this.getCoords().setY(destination.getY());
                    break;
                } else {
                    this.getCoords().setX(tempCell.getX());
                    this.getCoords().setY(tempCell.getY());
                }
            }
            //if choosed point is diagonal then change direction
            if (tempCell.getX() == diagonalPoint.getX() && tempCell.getY() == diagonalPoint.getY()) {
                this.setDirection(diagDir);
            }
        }
    }

    public void shootTorpedo(Coords destination) {

        //if current ship have enough torpedoes
        if (this.getTorpedoCapacity() > 0) {
            //shoot torpedo and decrease amount
            this.setTorpedoCapacity(this.getTorpedoCapacity() - 1);
            Torpedo trp = new Torpedo(this, destination);
        }
    }

    public void shootMissileOrRepair(Ship target) {

        //if current ship have enough missiles
        if (this.getMissileCapacity() > 0) {
            //if enemy is in shooting range
            if (isInShootingRange(target)) {
                //decrease missiles amount
                this.setMissileCapacity(this.getMissileCapacity() - 1);
                //if current ship can attack
                if (this.getSpecialisation() == 1) {
                    //if current ship hit target then
                    if (RandomGenerator.getPropabilityOf(this.getAcuracyRate())) {
                        for (int i = 0; i < Main.TEAM_1.size(); i++) {
                            //if current ship is in first team
                            if (this.getID() == Main.TEAM_1.get(i).getID()) {
                                makeDamage(1, target.getID(), this.getShootingPower());
                                break;
                            }
                        }
                        for (int i = 0; i < Main.TEAM_2.size(); i++) {
                            //if current ship is in second team
                            if (this.getID() == Main.TEAM_2.get(i).getID()) {
                                makeDamage(2, target.getID(), this.getShootingPower());
                                break;
                            }
                        }
                    }
                    //if ship have repair specialisation
                } else {
                    for (int i = 0; i < Main.TEAM_1.size(); i++) {
                        //if current ship is in first team
                        if (this.getID() == Main.TEAM_1.get(i).getID()) {
                            repair(1, target.getID());
                            break;
                        }
                    }
                    for (int i = 0; i < Main.TEAM_2.size(); i++) {
                        //if current ship is in second team
                        if (this.getID() == Main.TEAM_2.get(i).getID()) {
                            repair(2, target.getID());
                            break;
                        }
                    }
                }
            }
        }


    }

    private void makeDamage(int teamNum, int enemyID, int damage) {
        if (teamNum == 1) {
            for (int i = 0; i < Main.TEAM_2.size(); i++) {
                //if ship with same ID as enemy found, then it takes damage
                if (Main.TEAM_2.get(i).getID() == enemyID) {
                    Main.TEAM_2.get(i).setDurability(Main.TEAM_2.get(i).getDurability() - damage);
                }
            }
        } else {
            for (int i = 0; i < Main.TEAM_1.size(); i++) {
                //if ship with same ID as enemy found, then it takes damage
                if (Main.TEAM_1.get(i).getID() == enemyID) {
                    Main.TEAM_1.get(i).setDurability(Main.TEAM_1.get(i).getDurability() - damage);
                }
            }
        }

    }

    private void repair(int teamNum, int friendID) {

        if (teamNum == 1) {
            for (int i = 0; i < Main.TEAM_1.size(); i++) {
                //if ship with same ID as friend found, then
                if (Main.TEAM_1.get(i).getID() == friendID) {
                    //if ship have maximum health then do nothing
                    if (ReadParams.getDurabilities().get(Main.TEAM_1.get(i).getType() - 1) == Main.TEAM_1.get(i).getDurability()) {
                        //do nothing
                    } else {
                        //repair it with 25 percent of its maximum durability
                        Main.TEAM_1.get(i).setDurability(Main.TEAM_1.get(i).getDurability()
                                + (int) (ReadParams.getDurabilities().get(Main.TEAM_1.get(i).getType() - 1) * REPAIR_COEF));
                        //if after repair it have more then maximum health
                        if (ReadParams.getDurabilities().get(Main.TEAM_1.get(i).getType() - 1) <= Main.TEAM_1.get(i).getDurability()) {
                            //equate friend ship maximum health
                            Main.TEAM_1.get(i).setDurability(ReadParams.getDurabilities().get(Main.TEAM_1.get(i).getType() - 1));
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < Main.TEAM_2.size(); i++) {
                //if ship with same ID as friend found, then
                if (Main.TEAM_2.get(i).getID() == friendID) {
                    //if ship have maximum health then do nothing
                    if (ReadParams.getDurabilities().get(Main.TEAM_2.get(i).getType() - 1) == Main.TEAM_2.get(i).getDurability()) {
                        //do nothing
                    } else {
                        //repair it with 25 percent of its maximum durability
                        Main.TEAM_2.get(i).setDurability(Main.TEAM_2.get(i).getDurability()
                                + (int) (ReadParams.getDurabilities().get(Main.TEAM_2.get(i).getType() - 1) * REPAIR_COEF));
                        //if after repair it have more then maximum health
                        if (ReadParams.getDurabilities().get(Main.TEAM_2.get(i).getType() - 1) <= Main.TEAM_2.get(i).getDurability()) {
                            //equate friend ship maximum health
                            Main.TEAM_2.get(i).setDurability(ReadParams.getDurabilities().get(Main.TEAM_2.get(i).getType() - 1));
                        }
                    }
                }
            }
        }

    }

    public void ram(Ship enemy) {

        //enemy should be in ram range
        if (Math.abs(this.getCoords().getX() - enemy.getCoords().getX()) <= RAM_RANGE) {
            if (Math.abs(this.getCoords().getY() - enemy.getCoords().getY()) <= RAM_RANGE) {

                //if ram succeded
                if (RandomGenerator.getPropabilityOf((int) (RAM_SUCCESS_COEF * 100))) {
                    for (int i = 0; i < Main.TEAM_1.size(); i++) {
                        //if current ship is in first team
                        if (this.getID() == Main.TEAM_1.get(i).getID()) {
                            //damage enemy with current ship health value
                            makeDamage(1, enemy.getID(), this.getDurability());
                            break;
                        }
                    }
                    for (int i = 0; i < Main.TEAM_2.size(); i++) {
                        //if current ship is in second team
                        if (this.getID() == Main.TEAM_2.get(i).getID()) {
                            //damage enemy with current ship health value
                            makeDamage(2, enemy.getID(), this.getDurability());
                            break;
                        }
                    }
                }
                //kill current ship
                this.setDurability(0);

            }
        }

    }


    public void moveTillMissleShootingRange(Ship enemy) {

        //SR - shooting range
        int mostLeftSRPoint = 0, mostHighSRPoint = 0, mostRightSRPoint = 0, mostDownSRPoint = 0;
        //find left high point of vision area
        mostLeftSRPoint = this.getCoords().getX() - this.getShootingRange();
        mostHighSRPoint = this.getCoords().getY() - this.getShootingRange();
        //find right down point of vision area
        mostRightSRPoint = this.getCoords().getX() + this.getShootingRange();
        mostDownSRPoint = this.getCoords().getY() + this.getShootingRange();

        //if this point is out of war field, then it equals to default
        if (mostLeftSRPoint < Main.MIN_X) {
            mostLeftSRPoint = Main.MIN_X;
        }
        if (mostHighSRPoint < Main.MIN_Y) {
            mostHighSRPoint = Main.MIN_Y;
        }
        if (mostRightSRPoint > Main.MAX_X) {
            mostRightSRPoint = Main.MAX_X;
        }
        if (mostDownSRPoint > Main.MAX_Y) {
            mostDownSRPoint = Main.MAX_Y;
        }

        //if enemy is already in shooting range then do nothing
        if (enemy.getCoords().getX() <= mostRightSRPoint && enemy.getCoords().getX() >= mostLeftSRPoint &&
                enemy.getCoords().getY() <= mostDownSRPoint && enemy.getCoords().getY() >= mostHighSRPoint) {
            //do nothing
        } else {

            Coords destination = new Coords();

            //move ship till it cant attack enemy with missiles

            if ((this.getCoords().getX() < destination.getX()) && (this.getCoords().getY() >= destination.getY())) {
                //right top direction
                destination.setX(enemy.getCoords().getX() - this.getShootingRange());
                destination.setY(enemy.getCoords().getY() + this.getShootingRange());
            }
            if ((this.getCoords().getX() >= destination.getX()) && (this.getCoords().getY() > destination.getY())) {
                //left top direction
                destination.setX(enemy.getCoords().getX() + this.getShootingRange());
                destination.setY(enemy.getCoords().getY() + this.getShootingRange());
            }
            if ((this.getCoords().getX() > destination.getX()) && (this.getCoords().getY() <= destination.getY())) {
                //left bottom direction
                destination.setX(enemy.getCoords().getX() + this.getShootingRange());
                destination.setY(enemy.getCoords().getY() - this.getShootingRange());
            }
            if ((this.getCoords().getX() <= destination.getX()) && (this.getCoords().getY() < destination.getY())) {
                //right bottom direction
                destination.setX(enemy.getCoords().getX() - this.getShootingRange());
                destination.setY(enemy.getCoords().getY() - this.getShootingRange());
            }

            move(destination);
        }
    }

    public boolean isInShootingRange(Ship enemy) {
        //SR - shooting range
        int mostLeftSRPoint = 0, mostHighSRPoint = 0, mostRightSRPoint = 0, mostDownSRPoint = 0;
        //find left high point of vision area
        mostLeftSRPoint = this.getCoords().getX() - this.getShootingRange();
        mostHighSRPoint = this.getCoords().getY() - this.getShootingRange();
        //find right down point of vision area
        mostRightSRPoint = this.getCoords().getX() + this.getShootingRange();
        mostDownSRPoint = this.getCoords().getY() + this.getShootingRange();

        //if this point is out of war field, then it equals to default
        if (mostLeftSRPoint < Main.MIN_X) {
            mostLeftSRPoint = Main.MIN_X;
        }
        if (mostHighSRPoint < Main.MIN_Y) {
            mostHighSRPoint = Main.MIN_Y;
        }
        if (mostRightSRPoint > Main.MAX_X) {
            mostRightSRPoint = Main.MAX_X;
        }
        if (mostDownSRPoint > Main.MAX_Y) {
            mostDownSRPoint = Main.MAX_Y;
        }

        //if enemy is in shooting range then
        if (enemy.getCoords().getX() <= mostRightSRPoint && enemy.getCoords().getX() >= mostLeftSRPoint &&
                enemy.getCoords().getY() <= mostDownSRPoint && enemy.getCoords().getY() >= mostHighSRPoint) {
            return true;
        } else {
            return false;
        }
    }

    private Coords chooseShortest(Coords verticalPoint, Coords horizonPoint, Coords diagonalPoint, Coords destination) {

        Coords outCoord = new Coords();
        List<Coords> outList = new ArrayList<Coords>();
        double distValue = 0, minDist = 0;

        //find relevant coordinates
        if (!Coords.containsCoords(Main.BLOCKED_CELLS, verticalPoint)) {
            outList.add(verticalPoint);
        }
        if (!Coords.containsCoords(Main.BLOCKED_CELLS, horizonPoint)) {
            outList.add(horizonPoint);
        }
        if (!Coords.containsCoords(Main.BLOCKED_CELLS, diagonalPoint)) {
            outList.add(diagonalPoint);
        }

        //find minimum distance
        if (outList.size() > 0) {
            minDist = distance(outList.get(0), destination);
            outCoord = outList.get(0);
        }
        for (int i = 0; i < outList.size(); i++) {
            if (minDist > distance(outList.get(i), destination)) {
                minDist = distance(outList.get(i), destination);
                outCoord = outList.get(i);
            }
        }

        //if coordinates is out of borders, then change it to default
        if (outCoord.getX() < Main.MIN_X) {
            outCoord.setX(Main.MIN_X);
        }
        if (outCoord.getY() < Main.MIN_Y) {
            outCoord.setY(Main.MIN_Y);
        }
        if (outCoord.getX() > Main.MAX_X) {
            outCoord.setX(Main.MAX_X);
        }
        if (outCoord.getY() > Main.MAX_Y) {
            outCoord.setY(Main.MAX_Y);
        }

        //if all coordinates are blocked, make cell invalid
        if (outList.size() == 0) {
            System.out.println("Движение корабля заблокировано");
            War.dialog.log.setText(War.dialog.log.getText() + "\nДвижение корабля заблокировано");
            //outCoord.setX(-1);
            //outCoord.setY(-1);
        }

        //return calculated coords
        return outCoord;

    }

    public static double distance(Coords A, Coords B) {
        double outDist = 0;

        //calculate distance between A and B points
        outDist = Math.sqrt((B.getX() - A.getX()) * (B.getX() - A.getX()) + (B.getY() - A.getY()) * (B.getY() - A.getY()));

        return outDist;
    }

    public void focusOn(List<Integer> enemiesIDInVA, List<Ship> enemiesList) {
        //if at least one enemy is in vision
        if (enemiesIDInVA.size() > 0) {
            List<Double> weightCoefs = new ArrayList<Double>();
            //focus on enemy that is most relevant
            for (int i = 0; i < enemiesIDInVA.size(); i++) {
                //compose coeficient weights to find target by distance and health points
                weightCoefs.add(((getShipByID(enemiesIDInVA.get(i), enemiesList)).getDurability() * HP_WEIGHT) +
                        (distance(getCoords(), getShipByID(enemiesIDInVA.get(i), enemiesList).getCoords()) * DISTANCE_WEIGHT));
            }

            int minIdx = 0;
            double minVal = 10000;
            //find target with lowest coefficient
            for (int i = 0; i < weightCoefs.size(); i++) {
                //if ship is dead then do nothing
                if (getShipByID(enemiesIDInVA.get(i), enemiesList).getDurability() <= 0) {
                    //do nothing
                } else {
                    if (minVal > weightCoefs.get(i)) {
                        //hold index of weight coeficient
                        minVal = weightCoefs.get(i);
                        minIdx = i;
                    }
                }
            }


            //change focused target of current ship
            this.setCurrentTarget(getShipByID(enemiesIDInVA.get(minIdx), enemiesList));

            List<Ship> targList = new ArrayList<Ship>();
            for (int i = 0; i < enemiesIDInVA.size(); i++) {
                Ship s = getShipByID(enemiesIDInVA.get(i), enemiesList);
                if (s != null && s.getDurability() > 0)
                    targList.add(s);
            }

            this.setTargetList(targList);


        } else {
            this.setCurrentTarget(null);
            this.setTargetList(null);
        }

    }

    public static Ship getShipByID(int ID, List<Ship> shipList) {
        for (int i = 0; i < shipList.size(); i++) {
            //if request ID equals to the ID in list
            if (ID == shipList.get(i).getID()) {
                return shipList.get(i);
            }
        }
        //otherwise return null
        return null;
    }

    public int getShootingPower() {
        return shootingPower;
    }

    private void setShootingPower(int shootingPower) {
        this.shootingPower = shootingPower;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getVisionRange() {
        return visionRange;
    }

    private void setVisionRange(int visionRange) {
        this.visionRange = visionRange;
    }

    public int getShootingRange() {
        return shootingRange;
    }

    private void setShootingRange(int shootingRange) {
        this.shootingRange = shootingRange;
    }

    public int getAcuracyRate() {
        return acuracyRate;
    }

    private void setAcuracyRate(int acuracyRate) {
        this.acuracyRate = acuracyRate;
    }

    public int getSpeed() {
        return speed;
    }

    private void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Ship getCurrentTarget() {
        return currentTarget;
    }

    public void setCurrentTarget(Ship currentTarget) {
        this.currentTarget = currentTarget;
    }

    public int getTorpedoCapacity() {
        return torpedoCapacity;
    }

    private void setTorpedoCapacity(int torpedoCapacity) {
        this.torpedoCapacity = torpedoCapacity;
    }

    public int getMissileCapacity() {
        return missileCapacity;
    }

    public void setMissileCapacity(int missileCapacity) {
        this.missileCapacity = missileCapacity;
    }

    public List<Ship> getTargetList() {
        return targetList;
    }

    private void setTargetList(List<Ship> targetList) {
        this.targetList = targetList;
    }
}
