package com.gmail.starguest95.Gui;

/**
 * Created by tilic on 16.04.2017.
 */

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class Renderer extends DefaultTableCellRenderer {
    private int[] ships = new int[25];

    public Renderer(int[] ships) {
        this.ships = ships;
    }

    private ImageIcon icon0 = new ImageIcon("Images//00.png");
    private ImageIcon icon1 = new ImageIcon("Images//11.png");
    private ImageIcon icon2 = new ImageIcon("Images//22.png");
    private ImageIcon icon3 = new ImageIcon("Images//33.png");
    private ImageIcon icon4 = new ImageIcon("Images//44.png");
    private ImageIcon icon5 = new ImageIcon("Images//55.png");


    //Непосредственно отрисовка ячеек по столбцам, заливка и добавление изображения
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (column == 0) label.setBackground(new Color(240, 240, 240));
        else {
            if ((ships[row] == 0) || (Math.abs(ships[row]) == 5) || (Math.abs(ships[row]) == 10))
                label.setBackground(new Color(205, 255, 240));//new Color(240, 240, 240));
            else if (ships[row] > 0) label.setBackground(new Color(255, 200, 255));
            else label.setBackground(new Color(130, 255, 170));//Color.CYAN);
            switch (Math.abs(ships[row])) {
                //if (column==x)
                //switch (img) {
                case 0:
                    label.setIcon(icon0);
                    break;
                case 1:
                    label.setIcon(icon1);
                    break;
                case 2:
                    label.setIcon(icon2);
                    break;
                case 3:
                    label.setIcon(icon3);
                    break;
                case 4:
                    label.setIcon(icon4);
                    break;
                case 5:
                    label.setIcon(icon5);
                    break;
                case 10:
                    label.setIcon(null);
                    break;
            }
        }
        return label;

    }
}
