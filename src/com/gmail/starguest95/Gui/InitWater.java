package com.gmail.starguest95.Gui;

import javax.swing.*;
import java.awt.*;


/**
 * Created by tilic on 17.04.2017.
 * Класс вода - он включает в себя запуск функций нужных для изменения стандартного Renderer, работает по столбцам
 */

public class InitWater {
    public JTable InitW(int[][] ships) {
        JTable jt;

        String[] columns = new String[26];
        columns[0] = "";
        for (int i = 1; i < 26; i++)
            columns[i] = Integer.toString(i);

        String[][] data = new String[25][26];

        // Создаем таблицу
        jt = new JTable(data, columns) {
            // Не редактируемую
            public boolean isCellEditable(int data, int columns) {
                return false;
            }
        };

        jt.setPreferredScrollableViewportSize(new Dimension(26 * 21, 25 * 20));

        jt.setFillsViewportHeight(true);


        //для изменения высоты всех строк
        jt.setRowHeight(20);
        //и для столбцов указывал
        jt.getTableHeader().setResizingAllowed(false);
        jt.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS); //так красивее... можно AUTO_RESIZE_OFF
        jt.getColumnModel().getColumn(0).setCellRenderer(new com.gmail.starguest95.Gui.Renderer(ships[0]));
        for (int i = 0; i < 25; i++) {
            jt.getColumnModel().getColumn(i).setPreferredWidth(21);
            jt.getColumnModel().getColumn(i).setMaxWidth(21);
            data[i][0] = Integer.toString(i + 1);

            jt.getColumnModel().getColumn(i + 1).setCellRenderer(new com.gmail.starguest95.Gui.Renderer(ships[i]));


        }
        jt.getColumnModel().getColumn(25).setPreferredWidth(21);
        jt.getColumnModel().getColumn(25).setMaxWidth(21);

        return jt;
    }
}
