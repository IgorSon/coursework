package com.gmail.starguest95.Gui;

import com.gmail.starguest95.Main.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static com.gmail.starguest95.Main.Main.*;


/**
 * Created by tilic on 10.04.2017.
 * Война. Война никогда не меняется.
 */

public class War extends JDialog {
    private JPanel Panel;
    private JTable jt;
    private JButton close;
    private JPanel tab;
    private JButton goWar;
    private JButton round;
    private JPanel tab2;
    private JPanel tab3;
    public JTextArea log;
    private JScrollPane logpanel;


    private InitWater water = new InitWater();
    public int[][] ships = new int[25][25];
    public static War dialog = new War();

    // Здесь задается поведение и отрисовка окна приложения
    private War() {

        JTable ht;
        JTable ht2;

        setContentPane(Panel);
        setModal(true);


        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        close.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        round.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Round();
            }
        });

        goWar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GoWar();
            }
        });

        for (int i = 0; i < 25; i++)
            for (int j = 0; j < 25; j++)
                ships[i][j] = 0;

        round.setVisible(false);
        tab.setVisible(false);
        tab2.setVisible(false);
        tab3.setVisible(false);
        logpanel.setVisible(false);

        HelpTable HelpT1 = new HelpTable();
        ht = HelpT1.InitH1();

        this.tab2.setLayout(new BorderLayout());
        JScrollPane tableContainer2 = new JScrollPane(ht);
        this.tab2.add(tableContainer2, BorderLayout.CENTER);

        ht2 = HelpT1.InitH2();

        this.tab3.setLayout(new BorderLayout());
        JScrollPane tableContainer3 = new JScrollPane(ht2);
        this.tab3.add(tableContainer3, BorderLayout.CENTER);
    }


    private void display() {

        jt = water.InitW(ships);

        this.tab.updateUI();

        dialog.pack();

    }

    private void Round() {
        for (int i = 0; i < 25; i++)
            for (int j = 0; j < 25; j++)
                ships[i][j] = 0;
        act.step();

        int ii3 = torpedos.size();
        for (int i = 0; i < ii3; i++) {
            if (torpedos.get(i).isAlive())
                ships[torpedos.get(i).getCurrentCoords().getX()]
                        [torpedos.get(i).getCurrentCoords().getY()] = 5;
        }
        int ii1 = TEAM_1.size();
        for (int i = 0; i < ii1; i++) {
            if (TEAM_1.get(i).getDurability() > 0)
                ships[TEAM_1.get(i).getCoords().getX()]
                        [TEAM_1.get(i).getCoords().getY()] = TEAM_1.get(i).getType();
        }
        int ii2 = TEAM_2.size();
        for (int i = 0; i < ii2; i++) {
            if (TEAM_2.get(i).getDurability() > 0)
                ships[TEAM_2.get(i).getCoords().getX()]
                        [TEAM_2.get(i).getCoords().getY()] = -TEAM_2.get(i).getType();
        }

        display();


        int teamVictory = checkForWin();
        if (teamVictory != 0) {
            JOptionPane.showMessageDialog(null, "Победила команда " + teamVictory);
            System.out.println("Победила команда " + teamVictory);
        }

    }

    private void GoWar() {
        round.setVisible(true);
        tab.setVisible(true);
        tab2.setVisible(true);
        tab3.setVisible(true);
        logpanel.setVisible(true);
        goWar.setVisible(false);
        War.dialog.log.setText("");
        dialog.pack();


        int ii3 = torpedos.size();
        for (int i = 0; i < ii3; i++) {
            if (torpedos.get(i).isAlive())
                ships[torpedos.get(i).getCurrentCoords().getX()]
                        [torpedos.get(i).getCurrentCoords().getY()] = 5;
        }
        int ii1 = TEAM_1.size();
        for (int i = 0; i < ii1; i++) {
            if (TEAM_1.get(i).getDurability() > 0)
                ships[TEAM_1.get(i).getCoords().getX()]
                        [TEAM_1.get(i).getCoords().getY()] = TEAM_1.get(i).getType();
        }
        int ii2 = TEAM_2.size();
        for (int i = 0; i < ii2; i++) {
            if (TEAM_2.get(i).getDurability() > 0)
                ships[TEAM_2.get(i).getCoords().getX()]
                        [TEAM_2.get(i).getCoords().getY()] = -TEAM_2.get(i).getType();
        }

        dialog.pack();

        jt = water.InitW(ships);
        this.tab.setLayout(new BorderLayout());
        JScrollPane tableContainer = new JScrollPane(jt);
        this.tab.add(tableContainer, BorderLayout.CENTER);
        dialog.pack();

        //for (int i=0; i<25; i++)
        //    for (int j=0; j<25; j++)
        //        ships[i][j] = 0;

        int teamVictory = checkForWin();
        if (teamVictory != 0) {
            JOptionPane.showMessageDialog(null, "Победила команда " + teamVictory);
            System.out.println("Победила команда " + teamVictory);
        }
    }

    private void onCancel() {
        dispose();
    }


}