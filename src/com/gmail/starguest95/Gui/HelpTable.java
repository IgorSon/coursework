package com.gmail.starguest95.Gui;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by tilic on 17.04.2017.
 *  Хранение и отрисовка дополнительных информационных таблиц
 *  о составе и назначении флота и распределении команд игроков
 */


public class HelpTable {

    public JTable InitH1() {
        JTable ht;


        ImageIcon icon1 = new ImageIcon("Images//11.png");
        ImageIcon icon2 = new ImageIcon("Images//22.png");
        ImageIcon icon3 = new ImageIcon("Images//33.png");
        ImageIcon icon4 = new ImageIcon("Images//44.png");
        ImageIcon icon5 = new ImageIcon("Images//55.png");
        Object[][] data2 = {{icon1, "Однопалубник"}, {icon2, "Двухпалубник"}, {icon3, "Трёхпалубник"}, {icon4, "Ремонтник"}, {icon5, "Торпеда"}};
        String[] columns2 = {"", "Название"};
        ht = new JTable(data2, columns2) {
            public boolean isCellEditable(int data2, int columns2) {
                return false;
            }

            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };


        ht.setPreferredScrollableViewportSize(new Dimension(111, 100));

        //для изменения высоты всех строк
        ht.setRowHeight(20);
        //и для столбцов указывал
        ht.getTableHeader().setResizingAllowed(false);
        ht.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS); //так красивее... можно AUTO_RESIZE_OFF
        ht.getColumnModel().getColumn(0).setPreferredWidth(21);
        ht.getColumnModel().getColumn(0).setMaxWidth(21);

        return ht;
    }

    public JTable InitH2() {
        JTable ht2;

        Object[][] data3 = {{"", "Игрок 1"},{"", "Игрок 2"}};
        String[] columns3 = {"", "Игрок"};
        ht2 = new JTable(data3, columns3){
            public boolean isCellEditable(int data3, int columns3)
            {
                return false;
            }

            public Component prepareRenderer(
                    TableCellRenderer r, int data3, int columns3)
            {
                Component c = super.prepareRenderer(r, data3, columns3);

                if (columns3==0) {
                    if (data3 == 0)
                        c.setBackground(new Color(255, 200, 255));
                    else
                        c.setBackground(new Color(130, 255, 170));
                }
                else c.setBackground(new Color(240, 240, 240));
                return c;
            }
        };

        // Устанавливаем размер таблицы
        ht2.setPreferredScrollableViewportSize(new Dimension(111, 40));

        //для изменения высоты всех строк
        ht2.setRowHeight(20);
        //и для столбцов указывал
        ht2.getTableHeader().setResizingAllowed(false);
        ht2.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS); //так красивее... можно AUTO_RESIZE_OFF
        ht2.getColumnModel().getColumn(0).setPreferredWidth(21);
        ht2.getColumnModel().getColumn(0).setMaxWidth(21);

        return ht2;
    }
}
