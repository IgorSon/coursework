package com.gmail.starguest95.Strategy;

import com.gmail.starguest95.Main.Coords;
import com.gmail.starguest95.Main.Main;
import com.gmail.starguest95.ShipsUtils.Ship;
import com.gmail.starguest95.ShipsUtils.Torpedo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;


/**
 * Алгоритм выбора действия ИИ
 */
public class Strategy {
    public static boolean logging = false;

    // Тактика поведения боевого корабля
    private static void battleshipTactic(Ship curShip) {
        // Текущая цель
        Ship curTarget = curShip.getCurrentTarget();

        if (curShip.getCurrentTarget() != null) {

            // Если текущую цель потопили, выбираем новую
            if (curTarget.getDurability() <= 0) {
                if (curShip.getTargetList().size() > 1) {
                    List<Integer> idInVA = new ArrayList<>(curShip.getTargetList().size());
                    for (Ship s : curShip.getTargetList()) {
                        idInVA.add(s.getID());
                    }
                    curShip.focusOn(idInVA, curShip.getTargetList());
                    curTarget = curShip.getCurrentTarget();
                } else {
                    wandering(curShip);
                }

            }


            // Расстояние до цели, округленное вниз
            double distanceToTarget = Math.floor(Ship.distance(curShip.getCoords(), curTarget.getCoords()));
            // Если на расстоянии огня
            if (curShip.isInShootingRange(curTarget)) {
                // И снаряды ещё есть
                if (curShip.getMissileCapacity() > 0) {
                    // Пли!
                    curShip.shootMissileOrRepair(curTarget);
                    log(curShip, curTarget, "M");
                    return;
                }
                // Боезапас на нуле - тараним ублюдка
                if (distanceToTarget <= 1) {
                    curShip.ram(curTarget);
                    log(curShip, curTarget, "R");
                } else {
                    curShip.move(curTarget.getCoords());
                    log(curShip, curTarget.getCoords());
                }
                return;
            }
            // Если достаем, то пускаем торпеду
            if (curShip.getTorpedoCapacity() > 0 && distanceToTarget <= Torpedo.MAX_LIFETIME) {

                // Спикок союзников
                List<Ship> friendly = new ArrayList<Ship>();
                // Смотрим из какой команды корабль
                if (Main.TEAM_1.contains(curShip)) {
                    friendly = Main.TEAM_1;
                } else {
                    friendly = Main.TEAM_2;
                }

                // Смотрим, чтобы в радиусе поражения(2 клетки) не было союзных войск, и только тогда стреляем торпедой
                ListIterator<Ship> iter1 = friendly.listIterator();
                while (iter1.hasNext()) {
                    Ship temp = iter1.next();
                    if (temp.getDurability() > 0) {
                        if (Math.abs(curTarget.getCoords().getX() - temp.getCoords().getX()) > 2) {
                            if (Math.abs(curTarget.getCoords().getY() - temp.getCoords().getY()) > 2) {
                                curShip.shootTorpedo(curTarget.getCoords());
                                log(curShip, curTarget, "T");
                                return;
                            } else {
                                if (logging) {
                                    System.out.println("Рядом наш корабль!");
                                }

                            }
                        }
                    }
                }
            }
            // Иначе преследуем
            if (curShip.getMissileCapacity() > 0) {
                curShip.moveTillMissleShootingRange(curTarget);
                log(curShip, curTarget.getCoords());
            } else {
                curShip.move(curTarget.getCoords());
                log(curShip, curTarget.getCoords());
            }

        } else {
            // Если не видит врагов
            // Выбираем рандомную точку на карте, и двигаем туда. Режим поиска
            wandering(curShip);

        }
    }

    private static void wandering(Ship curShip) {
        Coords searchingTarget = new Coords();
        int a = 0; // Начальное значение диапазона - "от"
        int b = 25; // Конечное значение диапазона - "до"

        int randomCoord1 = a + (int) (Math.random() * b); // Генерация 1-го числа
        int randomCoord2 = a + (int) (Math.random() * b); // Генерация 2-го числа

        if (logging) {
            System.out.println("Случайные координаты: " + randomCoord1 + "," + randomCoord2);
        }
        searchingTarget.setX(randomCoord1);
        searchingTarget.setY(randomCoord2);
        //двигаем по случайным координатам
        curShip.move(searchingTarget);
    }

    // Тактика поведения ремонтного корабля
    private static void repairshipTactic(Ship curShip) {
        // Нечем чинить, тараним!
        if (curShip.getMissileCapacity() == 0) {
            double distanceToTarget = Math.floor(Ship.distance(curShip.getCoords(), curShip.getCurrentTarget().getCoords()));
            if (distanceToTarget <= 1) {
                curShip.ram(curShip.getCurrentTarget());
                log(curShip, curShip.getCurrentTarget(), "R");
            } else {
                curShip.move(curShip.getCurrentTarget().getCoords());
                log(curShip, curShip.getCurrentTarget().getCoords());
            }

        } else {
            // Список союзников
            List<Ship> friendly = new ArrayList<Ship>();
            // Их id
            List<Integer> ids = new ArrayList<>();
            // Смотрим из какой команды корабль
            if (Main.TEAM_1.contains(curShip)) {
                friendly = Main.TEAM_1;
            } else {
                friendly = Main.TEAM_2;
            }


            // Записываем id раненых кораблей
            ListIterator<Ship> iter = friendly.listIterator();
            while (iter.hasNext()) {
                Ship temp = iter.next();
                // Мертвому уже не поможешь
                if (temp.getDurability() <= 0) {
                    continue;
                }
                // Себя чинить не можем
                if (temp.getID() == curShip.getID()) {
                    continue;
                }
                if (temp.getType() == 1 && temp.getDurability() != 100) {
                    ids.add(temp.getID());
                    continue;
                }
                if (temp.getType() == 2 && temp.getDurability() != 200) {
                    ids.add(temp.getID());
                    continue;
                }
                if (temp.getType() == 3 && temp.getDurability() != 300) {
                    ids.add(temp.getID());
                }

            }
            // Если есть пациенты
            if (!ids.isEmpty()) {
                List<Double> distance = new ArrayList<>(friendly.size());
                List<Ship> damaged = new ArrayList<Ship>();
                for (int ID : ids) {
                    damaged.add(Ship.getShipByID(ID, friendly));
                }
                // Считаем расстояние до них
                for (Ship s : damaged) {
                    distance.add(Ship.distance(s.getCoords(), curShip.getCoords()));
                }

                // Чиним или едем к ближайшему
                int minIndex = distance.indexOf(Collections.min(distance));
                if (distance.get(minIndex) <= 1) {
                    curShip.shootMissileOrRepair(damaged.get(minIndex));
                    log(curShip, damaged.get(minIndex), "F");
                } else {
                    curShip.move(damaged.get(minIndex).getCoords());
                    log(curShip, damaged.get(minIndex).getCoords());
                }

            } else {
                // Если нет раненых, едем к самому крутому союзнику
                Ship s = null;
                for (Ship temp : friendly) {
                    if (temp.getDurability() <= 0)
                        continue;
                    if (temp.getType() == 3) {
                        s = temp;
                        break;
                    }
                    if (temp.getType() == 2) {
                        s = temp;
                    }
                    if (temp.getType() == 1 && s != null && s.getType() != 2) {
                        s = temp;
                    }
                }
                if (s != null) {
                    curShip.move(s.getCoords());
                    log(curShip, s.getCoords());
                } else {
                    // Если из команды остался один ремонтник, укрепляем его и пойдем на таран
                    curShip.setDurability(200);
                    curShip.setMissileCapacity(0);
                    log(curShip, curShip, "F");
                }
            }
        }

    }

    public static void makeDecision(List<Ship> team) {


        for (int i = 0; i < team.size(); i++) {


            // Zed is dead, baby
            if (team.get(i).getDurability() <= 0)
                continue;

            // Корабль боевой или ремонтник?
            if (team.get(i).getSpecialisation() == 1)
                battleshipTactic(team.get(i));
            else
                repairshipTactic(team.get(i));

            log(team.get(i));

        }


    }

    private static void log(Ship s) {
        if (logging) {
            System.out.println("-----------------------------------------");
            System.out.println("Корабль ID = " + s.getID() + " HP = " + s.getDurability());
            System.out.println("Координаты: " + s.getCoords().getAll() +
                    "    P: " + s.getMissileCapacity() + " T: " + s.getTorpedoCapacity());
            if (s.getCurrentTarget() != null) {
                System.out.println("Цель: " + s.getCurrentTarget().getID() +
                        " Дистанция: " + Math.floor(Ship.distance(s.getCoords(), s.getCurrentTarget().getCoords())));
            } else
                System.out.println("Цель не определена");
            System.out.println("-----------------------------------------");
        }
    }

    public static void log(List<Ship> s) {
        if (logging) {
            for (Ship ship : s) {
                log(ship);
            }
        }
    }


    private static void log(Ship s, Coords c) {
        if (logging) {
            System.out.println("Корабль " + s.getID() + " || Двигаюсь в " + c.getAll());
        }
    }

    private static void log(Ship s, Ship t, String str) {
        if (logging) {
            switch (str) {
                case "M":
                    System.out.println("Корабль " + s.getID() + " || Стреляю ракетой в " + t.getID());
                    break;
                case "T":
                    System.out.println("Корабль " + s.getID() + " || Пуск торпеды в " + t.getID());
                    break;
                case "R":
                    System.out.println("Корабль " + s.getID() + " || Тараним " + t.getID());
                    break;
                case "F":
                    System.out.println("Корабль " + s.getID() + " || Починка корабля " + t.getID());
            }
        }
    }


}
