package com.gmail.starguest95.SimulationUtils;

import com.gmail.starguest95.Main.Coords;
import com.gmail.starguest95.Main.Main;
import com.gmail.starguest95.ShipsUtils.Ship;
import com.gmail.starguest95.ShipsUtils.Torpedo;
import com.gmail.starguest95.Strategy.Strategy;

import java.util.ArrayList;
import java.util.List;

import static com.gmail.starguest95.Main.Main.*;


/**
 * Created by isv on 02.03.17.
 */
public class Actions {

    private static List<Integer> enemiesIDList = new ArrayList<Integer>();

    protected static List<Integer> detectEnemies(List<Ship> actingNavy, List<Ship> listOfEnemies){
        enemiesIDList.clear();
        //iterate through ships that alive and well
        for (int shipIdx = 0; shipIdx < actingNavy.size(); shipIdx++){
            //add vision area of each ship
            locateEnemyInShipVA(actingNavy.get(shipIdx), listOfEnemies);
        }

        return enemiesIDList;

    }

    public static void refreshBlockedCells(){
        //add coords of every ship in list
        for (int i = 0; i < TEAM_1.size(); i++){
            //first team
            Main.BLOCKED_CELLS.add(TEAM_1.get(i).getCoords());
        }
        //add coords of every ship in list
        for (int i = 0; i < TEAM_2.size(); i++){
            //second team
            Main.BLOCKED_CELLS.add(TEAM_2.get(i).getCoords());
        }
    }

    private static void locateEnemyInShipVA(Ship target, List<Ship> listOfEnemies){
        /*
         * VA - vision area
         * _____________
         *|_|_|_|_|_|_|_|
         *|_|_|_|_|_|_|_|
         *|_|_|_|_|_|_|_|
         *|_|_|_|*|_|_|_|
         *|_|_|_|_|_|_|_|
         *|_|_|_|_|_|_|_|
         *|_|_|_|_|_|_|_|
         * '*' - there ship is located, rest is a vision area for small ship
         */

        int mostLeftVAPoint = 0, mostHighVAPoint = 0, mostRightVAPoint = 0, mostDownVAPoint = 0;

        //find left high point of vision area
        mostLeftVAPoint = target.getCoords().getX() - target.getVisionRange();
        mostHighVAPoint = target.getCoords().getY() - target.getVisionRange();
        //find right down point of vision area
        mostRightVAPoint = target.getCoords().getX() + target.getVisionRange();
        mostDownVAPoint = target.getCoords().getY() + target.getVisionRange();


        //if this point is out of war field, then it equals to default
        if (mostLeftVAPoint < Main.MIN_X){
            mostLeftVAPoint = Main.MIN_X;
        }
        if (mostHighVAPoint < Main.MIN_Y){
            mostHighVAPoint = Main.MIN_Y;
        }
        if (mostRightVAPoint > Main.MAX_X){
            mostRightVAPoint = Main.MAX_X;
        }
        if (mostDownVAPoint > Main.MAX_Y){
            mostDownVAPoint = Main.MAX_Y;
        }

        //iterate through list of enemies to find coordinates that overlaps with vision area
        for (int shipIdx = 0; shipIdx < listOfEnemies.size(); shipIdx++){
            Coords coordTemp = new Coords();
            coordTemp.setY(listOfEnemies.get(shipIdx).getCoords().getY());
            coordTemp.setX(listOfEnemies.get(shipIdx).getCoords().getX());
            //is enemy coordinates is located in current ship vision area
            if ((coordTemp.getX() >= mostLeftVAPoint) && (coordTemp.getY() >= mostHighVAPoint)){
                if ((coordTemp.getX() <= mostRightVAPoint) && (coordTemp.getY() <= mostDownVAPoint)){
                    //if enemies list isn't already contains current ship ID
                    if (!enemiesIDList.contains(listOfEnemies.get(shipIdx).getID())) {
                        enemiesIDList.add(listOfEnemies.get(shipIdx).getID());
                    }
                }
            }
        }

    }

    public void step() {
        // Обновляем список клеток с препятствиями
        Actions.refreshBlockedCells();

        // Обновляем область видимости и фокус кораблей
        ThreadForTeam refreshVision = new ThreadForTeam();
        refreshVision.updateTeam(1);
        refreshVision.updateTeam(2);

        if (Strategy.logging) {
            System.out.println("!!!-----------------------------НОВЫЙ ХОД----------------------------------!!!");
        }

        // Мчатся торпеды
        torpedos.forEach(torpedo -> torpedo.act());
        // Выполняются действия для обеих команд
        Strategy.makeDecision(order);

        if (Strategy.logging) {
            for (Torpedo t : Main.torpedos) {
                if (t.isAlive()) {
                    System.out.println("Торпеда " + t.getID() + " Сейчас " + t.getCurrentCoords().getAll() +
                            " Летит в " + t.getDestinationCoords().getAll());
                }
            }
        }
    }

    private List<Ship> order = new ArrayList<Ship>();
    public void shipOrder(){
        // Очередность ходов кораблей: 1-ый кобаль 1-ой команды, 1-ый корабль 2-ой команды,
        // 2-ой корабль 1-ой команды, 2-ой корабль 2-ой команды и т.д.
        int k = 0, l = 0;
        while (k < TEAM_1.size() || l < TEAM_2.size()){
            if(k < TEAM_1.size())
                order.add(TEAM_1.get(k++));
            if(l < TEAM_2.size())
                order.add(TEAM_2.get(l++));
        }

    }





}
