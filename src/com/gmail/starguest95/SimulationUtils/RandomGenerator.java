package com.gmail.starguest95.SimulationUtils;

/**
 * Created by isv on 02.03.17.
 */
public class RandomGenerator {

    private static final int LOWER_BOUND = 0;
    private static final int UPPER_BOUND = 100;

    public static boolean getPropabilityOf(int propability) {
        //roll number in random generator
        int rolledNumber = LOWER_BOUND + (int) (Math.random() * UPPER_BOUND);
        //return true if succeded
        if (rolledNumber <= propability) {
            return true;
        }
        //return false if didnt hit propability
        return false;
    }

}
