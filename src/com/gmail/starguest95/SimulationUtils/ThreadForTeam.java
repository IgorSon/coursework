package com.gmail.starguest95.SimulationUtils;

import com.gmail.starguest95.Main.Main;

import java.util.ArrayList;
import java.util.List;

import static com.gmail.starguest95.SimulationUtils.Actions.detectEnemies;

/**
 * Created by isv on 02.03.17.
 * Обновление информации о противниках
 */
public class ThreadForTeam {

    protected List<Integer> enemiesIDList = new ArrayList<Integer>();
    private int teamNumber = 0;

    //teamNum - team number: 1 - first team, 2 - second team
    public void updateTeam(int teamNum) {
        enemiesIDList.clear();
        //detect enemies
        if (teamNum == 1) {
            enemiesIDList = detectEnemies(Main.TEAM_1, Main.TEAM_2);
        } else {
            enemiesIDList = detectEnemies(Main.TEAM_2, Main.TEAM_1);
        }

        //make focus for every ship
        if (teamNum == 1) {
            for (int i = 0; i < Main.TEAM_1.size(); i++) {
                Main.TEAM_1.get(i).focusOn(enemiesIDList, Main.TEAM_2);
            }
        } else {
            for (int i = 0; i < Main.TEAM_2.size(); i++) {
                Main.TEAM_2.get(i).focusOn(enemiesIDList, Main.TEAM_1);
            }
        }

    }


}
