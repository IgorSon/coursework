package com.gmail.starguest95.InOut;

import com.gmail.starguest95.Main.Main;
import com.gmail.starguest95.ShipsUtils.Ship;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by isv on 02.03.17.
 * this class is reading starting configuration for both teams
 */
public class ReadTeamConfig {

    private static final String CONFIG_PATH = "data/teamConfig.txt";
    private static final int SPACE_ASCII = 32;
    private static final int SEMICOLON_ASCII = 59;
    private static final int COMMA_ASCII = 44;
    private static int ID_STARTER = 0;

    public static void readTeamConfig() {

        List<String> lines = new ArrayList<String>();
        try {
            //read file into lines
            lines = Files.readAllLines(Paths.get(CONFIG_PATH), StandardCharsets.UTF_8);

            //parse strings into configuration for each team
            parseStringIntoConfig(lines.get(0), 1);
            parseStringIntoConfig(lines.get(1), 2);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void parseStringIntoConfig(String line, int teamID) {
        String signleShip = "";
        //loop for processing line of String
        for (int i = 0; i < line.length(); i++) {
            //if char is not a space
            if ((int) line.charAt(i) != SPACE_ASCII) {
                //compose a value
                signleShip += line.charAt(i);
            }
            if (((int) line.charAt(i) == SPACE_ASCII) || (i == line.length() - 1)) {
                //if ship parameters is composed
                if (signleShip.length() > 0) {

                    //add a new ship to the list
                    if (teamID == 1) {
                        Main.TEAM_1.add(returnShipFromString(signleShip));
                    }
                    if (teamID == 2) {
                        Main.TEAM_2.add(returnShipFromString(signleShip));
                    }

                    //release temp
                    signleShip = "";

                }
            }
        }
    }

    private static Ship returnShipFromString(String param) {
        String value = "";
        Ship outShip = new Ship();
        int semicolonAmount = 0, commaAmount = 0;
        for (int i = 0; i < param.length(); i++) {
            //if char is not a semicolon or comma
            boolean isASemicolon = ((int) param.charAt(i) == SEMICOLON_ASCII);
            boolean isAComma = ((int) param.charAt(i) == COMMA_ASCII);
            if (!((isASemicolon)
                    || (isAComma))) {
                //compose a value
                value += param.charAt(i);
            }
            //if it is separator symbol or end of the line
            if (isASemicolon || isAComma
                    || (i == param.length() - 1)) {
                //if value is composed
                if (semicolonAmount == 0) {
                    outShip.setType(Integer.valueOf(value));
                }
                //if value is an X coord
                if ((semicolonAmount == 1) && (commaAmount == 0)) {
                    outShip.getCoords().setX(Integer.valueOf(value));
                }
                //if value is an Y coord
                if ((semicolonAmount == 1) && (commaAmount == 1)) {
                    outShip.getCoords().setY(Integer.valueOf(value));
                }
                //if value is a direction
                if (semicolonAmount == 2) {
                    outShip.setDirection(value);
                }
                //if value is a specialization
                if (semicolonAmount == 3) {
                    outShip.setSpecialisation(Integer.valueOf(value));
                    //if this is repairing class ship, then it cant attack
                    if (Integer.valueOf(value) == 2) {
                        outShip.makeRepairClassShip();
                    }
                }

                //increase semicolon or comma amount
                if (((int) param.charAt(i) == SEMICOLON_ASCII) && (i != param.length() - 1)) {
                    semicolonAmount++;
                } else {
                    commaAmount++;
                }
                //get ID for each ship
                outShip.setID(getID());
                //release value
                value = "";
            }

        }
        //increase ID
        ID_STARTER++;
        return outShip;
    }

    private static int getID() {
        return ID_STARTER;
    }

}
