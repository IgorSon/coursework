package com.gmail.starguest95.InOut;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by isv on 01.03.17.
 * this class is reading inner parameters of ships, so further program dont have to be compiled
 * to change ship properties
 */
public class ReadParams {
    private static final String PARAMS_PATH = "data/params.txt";
    private static final int SPACE_ASCII = 32;

    //list of durability
    private static List<Integer> durabilities = new ArrayList<Integer>();
    //list of shooting power
    private static List<Integer> shootingPowerList = new ArrayList<Integer>();
    //list of vision range
    private static List<Integer> visionRangeList = new ArrayList<Integer>();
    //list of shooting range
    private static List<Integer> shootingRangeList = new ArrayList<Integer>();
    //list of acuracy rate
    private static List<Integer> acuracyRateList = new ArrayList<Integer>();
    //list of speed
    private static List<Integer> speedList = new ArrayList<Integer>();
    //list of torpedo capacity
    private static List<Integer> torpedoCapList = new ArrayList<Integer>();
    //list of missile capacity
    private static List<Integer> missileCapList = new ArrayList<Integer>();

    public static List<Integer> getDurabilities() {
        return durabilities;
    }

    public static List<Integer> getShootingPowerList() {
        return shootingPowerList;
    }

    public static List<Integer> getVisionRangeList() {
        return visionRangeList;
    }

    public static List<Integer> getShootingRangeList() {
        return shootingRangeList;
    }

    public static List<Integer> getAcuracyRateList() {
        return acuracyRateList;
    }

    public static List<Integer> getSpeedList() {
        return speedList;
    }

    public static void readParams() {
        List<String> lines = new ArrayList<String>();
        try {
            //read file into lines
            lines = Files.readAllLines(Paths.get(PARAMS_PATH), StandardCharsets.UTF_8);

            //read durabilities
            readStringValues(lines.get(0), 0);
            //read shooting power list
            readStringValues(lines.get(1), 1);
            //read vision range list
            readStringValues(lines.get(2), 2);
            //read shooting range list
            readStringValues(lines.get(3), 3);
            //read acuracy rate list
            readStringValues(lines.get(4), 4);
            //read speed list
            readStringValues(lines.get(5), 5);
            //read torpedo capacity line
            readStringValues(lines.get(6), 6);
            //read missile capacity line
            readStringValues(lines.get(7), 7);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readStringValues(String line, int listIdx) {
        String value = "";
        //loop for processing line of String
        for (int i = 0; i < line.length(); i++) {
            //if char is not a space
            if ((int) line.charAt(i) != SPACE_ASCII) {
                //compose a value
                value += line.charAt(i);
            }
            //if value is ready to be added
            if (((int) line.charAt(i) == SPACE_ASCII) || (i == line.length() - 1)) {
                //if value is composed
                if (value.length() > 0) {
                    //add it to the list
                    switch (listIdx) {
                        //durabilities line
                        case 0:
                            getDurabilities().add(Integer.valueOf(value));
                            break;
                        //shooting power line
                        case 1:
                            getShootingPowerList().add(Integer.valueOf(value));
                            break;
                        //vision range line
                        case 2:
                            getVisionRangeList().add(Integer.valueOf(value));
                            break;
                        //shooting range line
                        case 3:
                            getShootingRangeList().add(Integer.valueOf(value));
                            break;
                        //acuracy rate line
                        case 4:
                            getAcuracyRateList().add(Integer.valueOf(value));
                            break;
                        //speed line
                        case 5:
                            getSpeedList().add(Integer.valueOf(value));
                            break;
                        //torpedo capacity line
                        case 6:
                            getTorpedoCapacityList().add(Integer.valueOf(value));
                            break;
                        //missile capacity line
                        case 7:
                            getMissileCapList().add(Integer.valueOf(value));
                            break;
                    }
                    //release value temp
                    value = "";
                }
            }
        }
    }

    public static List<Integer> getTorpedoCapacityList() {
        return torpedoCapList;
    }

    public static List<Integer> getMissileCapList() {
        return missileCapList;
    }
}
